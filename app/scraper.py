import requests
from urllib.request import urlopen
from bs4 import BeautifulSoup
import re
import random

url = "https://en.wikipedia.org/wiki/Sky_Sports"
links = []

def scrapeURL(url):
    html_page = urlopen(url).read().decode('utf-8')
    soup = BeautifulSoup(html_page, features='lxml')

    href = soup.find_all('a')
    getLinks(href)
    input_info();



def getLinks(href):
 for link in href:
        url = link.get('href')

        if url != None and url.startswith('/wiki'):
            links.append('https://en.wikipedia.org'+url)

def input_info():
    page_url = random.choice(links)
    html = urlopen(page_url).read().decode('utf-8')
    soup = BeautifulSoup(html, features='lxml')
    # print(soupmix.find_all('a'))
    title = soup.select('h1')[0].text.strip()

    f = open("small-sample/"+ title+'.txt',"w+", encoding="utf-8")
    f.write(soup.text)
    f.close()

    print(title)
