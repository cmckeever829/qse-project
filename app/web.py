"""Search engine web server."""

import tiny
import scraper
from flask import Flask, render_template, request

my_index = tiny.Index("small-sample")
app = Flask(__name__)

@app.route("/")
def root():
    return render_template("index.html")

@app.route("/search")
def search():
    q = request.args['q']
    results = my_index.search(q)
    return render_template("results.html", q=q, results=results)

@app.route("/scrape")
def scrape():
    return render_template("scraper.html")

@app.route("/scraper")
def scrapeURL():
    q = request.args['q']
    print(q)
    scraper.scrapeURL(q)
    tiny.make_index("small-sample")
    return render_template('scrapeResults.html')
if __name__ == '__main__':
	app.run(debug=False,host='0.0.0.0',port=5000)
