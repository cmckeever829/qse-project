import unittest
import tiny

class TestTiny(unittest.TestCase):

    def testWords(self):
        self.assertEqual(tiny.words('Split Words'),['split', 'words'])
